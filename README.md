# YOLOv3 Model Bootstrapper

Automatically and interactively create a trainable YOLOv3 model with a custom dataset. No effort required!

First, you just need 3 things:

1. Your annotated images in a folder
2. `make` command (if you don't have make, you can manually move `yolov3gen.py` to a folder on your $PATH) and Python (preinstalled on Mac and Linux).
3. A Mac/Linux computer (Windows support coming soon).

To run this bootstrapper:

1. `sudo make install`
2. `yolov3gen /path/to/your/annotated/images`
