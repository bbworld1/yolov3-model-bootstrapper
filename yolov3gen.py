#!/usr/bin/env python3

# YOLOv3 bootstrapping script
# Yes, this is mostly os.system commands, deal with it
# I will never touch bash with a 10 foot pole

import os
import sys
import argparse
import mimetypes
import math
import shutil
import time

parser = argparse.ArgumentParser(description='Create a YOLOv3 model from custom data.')
parser.add_argument('data_location', help="Path to the dataset.")

args = parser.parse_args()

print("Generating YOLO model for dataset located in {}."
        .format(os.path.abspath(args.data_location)))


if os.path.isdir("darknet"):
    print("Using pre-cloned darknet base.")
else:
    confirmation = input("Cloning Darknet to {}. This will take up around 300M of space. Proceed? (y/n): "
            .format(os.getcwd())) 

    if confirmation.lower() == "y":
        os.system("git clone https://github.com/pjreddie/darknet.git")
        os.chdir("darknet/")
        os.system("curl -kLO https://pjreddie.com/media/files/darknet53.conv.74")
        print("-------------")
        print("Cloning complete.")
        print("-------------")
        print()
        os.chdir("../")
    else:
        print("Abort.")
        sys.exit(1)

print("Scanning annotated images...")
time.sleep(0.5)
count = 0
classes = set()

for entry in os.scandir(os.path.abspath(args.data_location)):
    if mimetypes.guess_type(entry.path)[0].split("/")[0] == "image":
        annotation_path = ".".join(entry.path.split(".")[:-1] + ["txt"])
        if os.path.isfile(annotation_path):
            count += 1
        else:
            print("No YOLO-compatible annotation found for {}, skipping."
                    .format(entry.path))

if count == 0:
    print("No YOLO-compatible annotations found. " +\
            "If you saved your annotations in VOC XML format use converter.py.")
    sys.exit(1)

print("{} annotated images found.".format(count))
print("Saving train.txt...")
print("Saving test.txt...")
trainfile = open(os.path.abspath(args.data_location) + "/train.txt", "w")
testfile = open(os.path.abspath(args.data_location) + "/test.txt", "w")
for c,entry in enumerate(os.scandir(os.path.abspath(args.data_location))):

    if mimetypes.guess_type(entry.path)[0].split("/")[0] == "image":
        annotation_path = ".".join(entry.path.split(".")[:-1] + ["txt"])
        if os.path.isfile(annotation_path):
            if c <= math.floor(count * 0.9):
                # Save to train data
                trainfile.write(entry.path + "\n")
            else:
                # Switch to test data
                testfile.write(entry.path + "\n")
        else:
            pass

trainfile.close()
testfile.close()
print("-------------")
print("Scanning complete.")
print("-------------")
print()
print("Setting up YOLO configs...")
datafile = open("darknet/cfg/obj.data", "w")
namesfile = open("darknet/cfg/obj.names", "w")
print("Looking for classes.txt...")
if os.path.isfile(args.data_location + "/classes.txt"):
    print("Found!")
    classes = open(args.data_location + "/classes.txt")
    classcount = 0
    for c in classes:
        namesfile.write(c + "\n")
        classcount += 1
    classes.close()
    namesfile.close()
    datafile.write("classes = {}\n".format(classcount))
    datafile.write("train = {}/train.txt\n".format(os.path.abspath(args.data_location)))
    datafile.write("valid = {}/test.txt\n".format(os.path.abspath(args.data_location)))
    datafile.write("names = obj.names\n")
    datafile.write("backup = backup/\n")
    datafile.close()

else:
    print("Not found. Please specify your classes in {}/classes.txt."
            .format(args.data_location))
    sys.exit(1)

model_choice = ""
while model_choice not in ["1", "2"]:
    model_choice = input("Choose 1 for yolov3-tiny or 2 for yolov3: ")
    if model_choice not in ["1", "2"]:
        print("Invalid, try again:")

print(classcount)
if model_choice == "1":
    with open("darknet/cfg/yolov3-tiny.cfg") as f:
        cfg = f.readlines()
    cfg[2] = "batch=24\n"
    cfg[3] = "subdivisions=16\n"
    cfg[126] = "filters={}\n".format((classcount+5)*3)
    cfg[134] = "classes={}\n".format(classcount)
    cfg[170] = "filters={}\n".format((classcount+5)*3)
    cfg[176] = "classes={}\n".format(classcount)
elif model_choice == "2":
    with open("darknet/cfg/yolov3.cfg") as f:
        cfg = f.readlines()
    cfg[2] = "batch=24\n"
    cfg[3] = "subdivisions=16\n"
    cfg[602] = "filters={}\n".format((classcount+5)*3)
    cfg[609] = "classes={}\n".format(classcount)
    cfg[688] = "filters={}\n".format((classcount+5)*3)
    cfg[695] = "classes={}\n".format(classcount)
    cfg[775] = "filters={}\n".format((classcount+5)*3)
    cfg[782] = "classes={}\n".format(classcount)

print(cfg)
with open("darknet/cfg/train.cfg", "w") as f:
    cfg = f.writelines(cfg)

print("Compiling YOLO...")
cuda = input("Do you want CUDA acceleration? (y/n): ")
cudnn = input("Do you want CUDNN acceleration? (y/n): ")
openmp = input("Do you want OpenMP acceleration? (y/n): ")
opencv = input("Do you want OpenCV integration? (y/n): ")

with open("darknet/Makefile") as makefile:
    mftext = makefile.readlines()

if cuda.lower() == "y":
    mftext[0] = "GPU=1\n"
if cudnn.lower() == "y":
    mftext[1] = "CUDNN=1\n"
if openmp.lower() == "y":
    mftext[3] = "OPENMP=1\n"
if opencv.lower() == "y":
    mftext[2] = "OPENCV=1\n"

with open("darknet/Makefile", "w") as makefile:
    makefile.writelines(mftext)

os.chdir("darknet")
os.system("make clean")
os.system("make")
print("-------------")
print("Compilation complete.")
print("-------------")
print()
os.chdir("..")
os.chdir("darknet")
print("Training (weights saved to backup/ every 1000 iterations)...")
print("Ctrl-C at any time to stop the training. YOLO will train endlessly.")
os.system("./darknet detector train cfg/obj.data cfg/train.cfg darknet53.conv.74")
